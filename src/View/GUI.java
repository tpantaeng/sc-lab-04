package View;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import model.nested;

public class GUI extends JFrame{
	private JLabel showProjectName;
	private JFrame frame;
	private JButton run;
	private JTextField textField;
	private JComboBox comboBox;
	private JTextArea textArea;

	public GUI() {
		createframe();
	}

	public void createframe() {
		// TODO Auto-generated method stub
		showProjectName = new JLabel("LAB4");
		 setLayout(null);
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Nested loop 1", "Nested loop 2", "Nested loop 3", "Nested loop 4"}));
		comboBox.setBounds(123, 31, 121, 20);
		add(comboBox);
		textField = new JTextField();
		textField.setBounds(45, 84, 86, 20);
		add(textField);
		
		run = new JButton("RUN");
		run.setBounds(42, 127, 89, 23);
		add(run);
		textArea = new JTextArea();
		textArea.setBounds(205, 84, 169, 266);
		add(textArea);
		run.addActionListener(new ActionListener() {

		
	
	public void setProjectName(String s) {
		   showProjectName.setText(s);
	}
	public void buttonListener(ActionListener list){
		run.addActionListener(list);
	}
	
	public void actionPerformed(ActionEvent e){
		String nes ="";
		int loop = Integer.parseInt(textField.getText());
		nested nest = new nested();
		nes = comboBox.getSelectedItem().toString();
		if(nes == "Nested loop 1"){
			textArea.setText(nest.nested1(loop));}
		else if(nes == "Nested loop 2"){
			textArea.setText(nest.nested2(loop));}
		else if(nes == "Nested loop 3"){
			textArea.setText(nest.nested3(loop));}
		else if(nes == "Nested loop 4"){
			textArea.setText(nest.nested4(loop));}
	}
});
	
	
	
	}
}
